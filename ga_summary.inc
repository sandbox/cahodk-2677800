<?php
/**
 * @file
 * Manages integration between the ga summary queue items and google analytics.
 */

/**
 * Class GASummary.
 *
 * Manages integration between the ga summary queue items and google analytics.
 */
class GASummary {

  protected static $instance;

  /**
   * GASummary constructor.
   *
   * Left empty for singleton pattern.
   */
  protected function __construct(){}

  /**
   * GASummary clone.
   *
   * Left empty for singleton pattern.
   */
  protected function __clone(){}

  /**
   * Static class instantiation.
   *
   * @return mixed
   *   returns the singleton.
   */
  public static function get_instance() {
    if (empty(static::$instance)) {
      static::$instance = new static();
      module_load_include('inc', 'google_analytics_reports');
      static::$instance->available_fields = google_analytics_reports_get_fields();
    }
    return static::$instance;
  }

  /**
   * Return every pagePath from google analytics.
   *
   * @return array
   *   An array of the original and trimmed pagePath.
   */
  public function get_pages() {
    $view = $this->get_pages_view();
    $view->execute();
    $result = $view->result;

    $pages = array();
    foreach ($result as $page) {
      $page_path = preg_split("/\?|\./", $page->pagePath);
      $page_path = $page_path[0];
      $page_path = ltrim($page_path, '/');
      if (!in_array($page_path, $pages)) {
        $page_object = array(
          'original' => $page->pagePath,
          'short' => $page_path,
        );
        $pages[] = $page_object;
      }
    }

    return $pages;
  }

  /**
   * Returns every metric checked on the ga summary config page.
   *
   * @return array
   *   Checked datatypes.
   */
  public function get_ga_fields() {
    $fields = array();
    foreach (static::$instance->available_fields as $key => $field) {
      if (variable_get('ga_summary_' . $key)) {
        $fields[$key] = $field;
      }
    }
    if (count($fields) > 0) {
      return $fields;
    }
  }

  /**
   * Saves google analytics data in the database.
   *
   * @param $ga_object
   *   array containing rows of ga data, trimmed and original pagePath.
   * @param $entity
   *   Either a node or a taxonomy term.
   */
  public function save_ga_object($ga_object, $entity) {
    try {
      // Get ID of existing summary node.
      $id = db_select('ga_summary_node', 'n')
        ->condition('page_path', $ga_object['page']['original'], '=')
        ->fields('n', array('id'))
        ->range(0, 1)
        ->execute()
        ->fetchAssoc();
      $id = $id['id'];

      $fields = array(
        'nid' => (isset($entity['entity']->nid)) ? $entity['entity']->nid : NULL,
        'tid' => (isset($entity['entity']->tid)) ? $entity['entity']->tid : NULL,
        'uid' => ($entity['type'] == 'node') ? $entity['entity']->uid : NULL,
        'type' => $entity['type'],
        'page_path' => $ga_object['page']['original'],
      );

      // Update or insert GA summary node.
      if (!$id) {
        $id = db_insert('ga_summary_node')
          ->fields($fields)
          ->execute();
      }
      else {
        db_update('ga_summary_node')
          ->fields($fields)
          ->condition('id', $id, '=')
          ->execute();
      }

      // Insert data.
      foreach ($ga_object['rows'] as $row) {
        foreach ($row as $key => $metric) {
          $field = static::$instance->available_fields[$key];

          $fields = array(
            'machine_name' => $key,
            'name' => $field['name'],
            'grouping' => $field['group'],
            'ga_summary_node_id' => $id,
            'value' => $metric,
          );

          db_insert('ga_summary_datatype')
            ->fields($fields)
            ->execute();
        }
      }
    }
    catch (\Exception $e) {
      watchdog('ga_summary', $e);
    }
  }

  /**
   * Import data from Google Analytics through google analytics reports API.
   *
   * @param array $page_object
   *   array of trimmed and original pagePath.
   * @param array $entity
   *   Node or taxonomy term.
   * @param array $fields
   *   ga_summary_datatypes to update
   *
   * @return array
   *   Array of GAObjects
   */
  public function import_ga_data($page_object, $entity, $fields) {
    $ga_object = array(
      'page' => $page_object,
      'rows' => array(),
    );

    if (!empty($fields)) {
      // The metrics should be part of every lookup.
      $metrics = array();
      foreach ($fields as $i => $field) {
        if ($field['type'] == 'metric') {
          $metrics[$i] = $field;
          unset($fields[$i]);
        }
      }

      // We can only ask the google analytics API for 7 metrics at a time.
      $limit = 7 - count($metrics);

      $field_chunks = array_chunk($fields, $limit, TRUE);
      $i = 0;
      do {
        $field_chunk = $field_chunks[$i];
        $chunk = $this->get_page_data_chunk_view($page_object['original'], $entity, $field_chunk, $metrics);
        if (!empty($chunk)) {
          $this->chunk_object_to_array($chunk, $ga_object['rows']);
        }
        ++$i;
      } while ($i < count($field_chunks));
    }

    return $ga_object;
  }

  /**
   * Convert a chunk to an array.
   *
   * @param $chunk
   *   Chunk of data for the object_rows.
   * @param $object_rows
   *   The total rows.
   */
  protected function chunk_object_to_array($chunk, &$object_rows) {
    foreach ($chunk as $i => $row) {
      foreach ($row as $key => $field) {
        $object_rows[$i][$key] = $field;
      }
    }
  }

  /**
   * @param $page_path
   * @param $entity
   * @param $dimensions
   * @param $metrics
   * @return array
   */
  protected function get_page_data_chunk_view($page_path, $entity, $dimensions, $metrics) {
    $view = new view();

    $view->base_table = 'google_analytics';
    $handler = $view->new_display('default');
    $handler->display->display_options['cache']['type'] = 'none';
    $handler->display->display_options['query']['type'] = 'views_query';

    if (count($dimensions)) {
      foreach ($dimensions as $key => $dimension) {
        $handler->display->display_options['fields'][$key]['id'] = $key;
        $handler->display->display_options['fields'][$key]['table'] = 'google_analytics';
        $handler->display->display_options['fields'][$key]['field'] = $key;
      }
    }

    foreach ($metrics as $key => $metric) {
      $handler->display->display_options['fields'][$key]['id'] = $key;
      $handler->display->display_options['fields'][$key]['table'] = 'google_analytics';
      $handler->display->display_options['fields'][$key]['field'] = $key;
    }

    /* Filterkriterie: Page tracking dimensions: Side */
    $handler->display->display_options['filters']['pagePath']['id'] = 'pagePath';
    $handler->display->display_options['filters']['pagePath']['table'] = 'google_analytics';
    $handler->display->display_options['filters']['pagePath']['field'] = 'pagePath';
    $handler->display->display_options['filters']['pagePath']['value'] = $page_path;

    $view->execute();
    $result = $view->result;

    if (count($result) > 0) {
      return $result;
    }
  }

  /**
   * Get all pagePaths from google analytics.
   *
   * @return \view
   */
  protected function get_pages_view() {
    $view = new view();
    $view->base_table = 'google_analytics';
    $handler = $view->new_display('default');
    $handler->display->display_options['cache']['type'] = 'none';
    $handler->display->display_options['query']['type'] = 'views_query';

    /* Felt: Page tracking dimensions: Side */
    $handler->display->display_options['fields']['pagePath']['id'] = 'pagePath';
    $handler->display->display_options['fields']['pagePath']['table'] = 'google_analytics';
    $handler->display->display_options['fields']['pagePath']['field'] = 'pagePath';

    /* Filterkriterie: Google Analytics: Start date of report */
    $handler->display->display_options['filters']['start_date']['id'] = 'start_date';
    $handler->display->display_options['filters']['start_date']['table'] = 'google_analytics';
    $handler->display->display_options['filters']['start_date']['field'] = 'start_date';
    $handler->display->display_options['filters']['start_date']['value']['value'] = '-31 day';
    $handler->display->display_options['filters']['start_date']['value']['type'] = 'offset';

    /* Filterkriterie: Google Analytics: End date of report */
    $handler->display->display_options['filters']['end_date']['id'] = 'end_date';
    $handler->display->display_options['filters']['end_date']['table'] = 'google_analytics';
    $handler->display->display_options['filters']['end_date']['field'] = 'end_date';
    $handler->display->display_options['filters']['end_date']['value']['value'] = 'now';
    $handler->display->display_options['filters']['end_date']['value']['type'] = 'offset';

    return $view;
  }

  /**
   * Convert google analytics pagePath to Node or term.
   *
   * @param $page_path
   * @return array
   */
  public function load_node_from_path($page_path) {
    if ($path = drupal_lookup_path("source", $page_path)) {
      $explode_path = explode('/', $path);
      $entity = array(
        'type' => $explode_path[0],
      );
      switch ($entity['type']) {
        case 'node':
          $entity['entity'] = menu_get_object("node", 1, $path);
          return $entity;
          break;

        case 'taxonomy':
          $entity['entity'] = taxonomy_term_load(array_pop(explode('/', $path)));
          return $entity;
          break;

        default:
          break;

      }
    }
  }

}
