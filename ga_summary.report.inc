<?php
/**
 * @file
 * Renders the google summary table.
 */

/**
 * Display all google metrics for every node created by current user.
 */
function ga_summary_report_form($form, &$form_state) {
  global $user;


  $query = db_select('ga_summary_node', 'g')
    ->condition('g.uid', 33469, '=');
  $query->leftJoin('ga_summary_data', 'd', 'g.id = d.ga_summary_node_id');
  $query->leftJoin('ga_summary_datatype', 't', 't.id = d.ga_summary_datatype_id');
  $query->leftJoin('node', 'n', 'n.nid = g.nid');
  $query->fields('g')->fields('d')->fields('t')->fields('n');

  $metrics = $query->execute();

  if ($metrics->rowCount() == 0) {
    $form['warning'] = array(
      '#type' => 'markup',
      '#markup' => '<p>No results found.</p>',
    );
  }

  $header = array();
  $rows = array();

  $row = array();
  while ($metric = $metrics->fetchAssoc()) {

    // Ignore pagePath since we display the node URL.
    if ($metric['machine_name'] === 'pagePath') {
      continue;
    }

    if (!isset($row['title'])) {
      $header[] = t('Title');
      $row['title'] = l($metric['title'], 'node/' . $metric['nid']);
    }

    if (!isset($header[$metric['machine_name']])) {
      $header[$metric['machine_name']] = $metric['name'];
    }

    $row[$metric['machine_name']] = $metric['value'];
  }
  $rows[] = $row;

  $form['table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
  );

  return $form;
}
