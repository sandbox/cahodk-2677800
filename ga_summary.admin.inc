<?php
/**
 * @file
 * GA Summary configuration form.
 */

/**
 * Implements hook_admin_settings().
 */
function ga_summary_admin_settings_form($form, &$form_state) {
  module_load_include('inc', 'google_analytics_reports');
  $available_fields = google_analytics_reports_get_fields();

  $form['ga_summary_end_date'] = array(
    '#type' => 'textfield',
    '#title' => t('End date (days from today - usually you want zero here)'),
    '#default_value' => variable_get('ga_summary_end_date', 0),
    '#element_validate' => array('ga_summary_element_validate_integer'),
  );

  $form['ga_summary_start_date'] = array(
    '#type' => 'textfield',
    '#title' => t('Start date (days from today - 31 should be fine)'),
    '#default_value' => variable_get('ga_summary_start_date', 31),
    '#element_validate' => array('ga_summary_element_validate_integer'),
  );

  foreach ($available_fields as $key => $field) {

    if (empty($form[$field['type']])) {
      $form[$field['type']] = array(
        '#type' => 'fieldset',
        '#title' => $field['type'],
      );
    }

    if (empty($form[$field['type']][$field['group']])) {
      $form[$field['type']][$field['group']] = array(
        '#type' => 'fieldset',
        '#title' => $field['group'],
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
      );
    }

    $var = 'ga_summary_' . $key;
    $form[$field['type']][$field['group']][$var] = array(
      '#type' => 'checkbox',
      '#title' => $field['name'],
      '#default_value' => variable_get($var, 0),
    );
  }

  return system_settings_form($form);
}

/**
 * Validate start and end date.
 *
 * @param array $elem
 *   Current element under validation.
 * @param array $form_state
 *   Form_state.
 */
function ga_summary_element_validate_integer(array $elem, array &$form_state) {
  if (!preg_match("/^\d*$/", trim($elem['#value']))) {
    form_error($elem, t('Variable should be a positive number or zero.'));
  }

  if ($elem['#name'] == "ga_summary_end_date") {
    if ($elem['#value'] >= $form_state['values']['ga_summary_start_date']) {
      form_error($elem, t('The end date should be less than the start date.'));
    }
  }
}
