<?php
/**
 * @file
 * Description.
 */

/**
 * Implements hook_views_data().
 */
function ga_summary_views_data() {
  module_load_include('inc', 'google_analytics_reports');
  $fields = google_analytics_reports_get_fields();

  $data['ga_summary_node'] = array(
    'table' => array(
      'group' => t('GA Summary'),
      'base' => array(
        'title' => t('GA Summary'),
        'help' => t('Contains records we want exposed to Views.'),
      ),
      'join' => array(
        'taxonomy_term_data' => array(
          'left_field' => 'tid',
          'field' => 'tid',
        ),
        'user' => array(
          'left_field' => 'uid',
          'field' => 'uid',
        ),
      ),
    ),
    'type' => array(
      'title' => t('Type'),
      'help' => t('Type.'),

      'field' => array(
        // ID of field handler plugin to use.
        'id' => 'string',
      ),

      'sort' => array(
        // ID of sort handler plugin to use.
        'id' => 'standard',
      ),

      'filter' => array(
        // ID of filter handler plugin to use.
        'id' => 'string',
      ),

      'argument' => array(
        // ID of argument handler plugin to use.
        'id' => 'string',
      ),
    ),
    'nid' => array(
      'title' => t('Node ID'),
      'help' => t('The record node ID.'),
      'field' => array(
        'handler' => 'views_handler_field_node',
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_numeric',
      ),
      'relationship' => array(
        'base' => 'node',
        'field' => 'nid',
        'handler' => 'views_handler_relationship',
        'label' => t('Node'),
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_node_nid',
        'numeric' => TRUE,
        'validate type' => 'nid',
      ),
      'relationship' => array(
        'title' => t('Node'),
        'help' => t('Relate content to the user who created it. This relationship will create one record for each content item created by the user.'),
        'handler' => 'views_handler_relationship',
        'base' => 'node',
        'base field' => 'nid',
        'field' => 'nid',
        'label' => t('nodes'),
      ),
    ),
    'tid' => array(
      'title' => t('Term ID'),
      'help' => t('The tid of a taxonomy term.'),
      'field' => array(
        'handler' => 'views_handler_field_numeric',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_taxonomy',
        'name field' => 'name',
        'zero is null' => TRUE,
      ),
      'filter' => array(
        'title' => t('Term'),
        'help' => t('Taxonomy term chosen from autocomplete or select widget.'),
        'handler' => 'views_handler_filter_term_node_tid',
        'hierarchy table' => 'taxonomy_term_hierarchy',
        'numeric' => TRUE,
      ),
    ),
    'uid' => array(
      'title' => t('Uid'),
      'help' => t('The user ID'), // The help that appears on the UI,
      'field' => array(
        'handler' => 'views_handler_field_user',
        'click sortable' => TRUE,
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_user_uid',
        'name field' => 'name', // display this field in the summary
      ),
      'filter' => array(
        'title' => t('User ID'),
        'handler' => 'views_handler_filter_user_current',
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
    ),
  );

  $data['ga_summary_datatype'] = array(
    'table' => array(
      'group' => t('GA Summary'),
      'base' => array(
        'title' => t('GA Summary'),
        'help' => t('Contains records we want exposed to Views.'),
      ),
      'join' => array(
        'ga_summary_node' => array(
          'left_field' => 'id',
          'field' => 'ga_summary_node_id',
        ),
      ),
    ),
    'value' => array(
      'title' => t('Value'),
      'help' => t('Just a numeric field.'),

      'field' => array(
        // ID of field handler plugin to use.
        'id' => 'numeric',
      ),

      'sort' => array(
        // ID of sort handler plugin to use.
        'id' => 'standard',
      ),

      'filter' => array(
        // ID of filter handler plugin to use.
        'id' => 'numeric',
      ),

      'argument' => array(
        // ID of argument handler plugin to use.
        'id' => 'numeric',
      ),
    ),
    'name' => array(
      'title' => t('Data type'),
      'help' => t('Just a numeric field.'),

      'field' => array(
        // ID of field handler plugin to use.
        'id' => 'string',
      ),

      'sort' => array(
        // ID of sort handler plugin to use.
        'id' => 'standard',
      ),

      'filter' => array(
        // ID of filter handler plugin to use.
        'id' => 'string',
      ),

      'argument' => array(
        // ID of argument handler plugin to use.
        'id' => 'string',
      ),
    ),
  );

  return $data;
}
